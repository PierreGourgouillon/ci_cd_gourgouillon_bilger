# Docker BDD + IHM WEB
docker pull mysql : Télécharge l'image Docker officielle de MySQL depuis Docker Hub.

docker pull phpmyadmin : Télécharge l'image Docker officielle de phpMyAdmin depuis Docker Hub.

docker run --network docker --hostname mysql-container -p 5200:3306 -e MYSQL_ROOT_PASSWORD=myPassword mysql : Lance un conteneur basé sur l'image MySQL, sur le réseau Docker nommé `docker`, avec le nom d'hôte `mysql-container`, expose le port 3306 sur le port 5200 de l'hôte, et définit la variable d'environnement `MYSQL_ROOT_PASSWORD` pour le mot de passe root de MySQL.

docker run --network docker --hostname phpmyadmin-container --link mysql-container:db -p 8080:80 phpmyadmin : Lance un conteneur basé sur l'image phpMyAdmin, sur le réseau Docker nommé `docker`, avec le nom d'hôte `phpmyadmin-container`, lie le conteneur MySQL (`mysql-container`) sous l'alias `db`, et expose le port 80 sur le port 8080 de l'hôte.

# Docker BDD + Wallabag

docker pull wallabag/wallabag: Télécharge l'image Docker officielle de Wallabag depuis Docker Hub.

docker run --network docker --hostname mysql-container -p 5200:3306 -e MYSQL_ROOT_PASSWORD=myPassword mysql : Lance un conteneur basé sur l'image MySQL, sur le réseau Docker nommé `docker`, avec le nom d'hôte `mysql-container`, expose le port 3306 sur le port 5200 de l'hôte, et définit la variable d'environnement `MYSQL_ROOT_PASSWORD` pour le mot de passe root de MySQL.

docker run --network docker -e MYSQL_ROOT_PASSWORD=myPassword --link mysql-container:db -p 8080:80 wallabag/wallabag: : Lance un conteneur basé sur l'image `wallabag/wallabag`, sur le réseau Docker nommé `docker`, avec la variable d'environnement `MYSQL_ROOT_PASSWORD` définie à `myPassword`, lie le conteneur nommé `mysql-container` sous l'alias `db`, et expose le port 80 du conteneur sur le port 8080 de l'hôte.

# Docker Mysql + Mariadb + PhpMyAdmin

docker run --network docker --hostname mysql-container -p 5200:3306 -e MYSQL_ROOT_PASSWORD=myPassword mysql: Lance un conteneur basé sur l'image MySQL, sur le réseau Docker nommé `docker`, avec le nom d'hôte `mysql-container`, expose le port 3306 sur le port 5200 de l'hôte, et définit la variable d'environnement `MYSQL_ROOT_PASSWORD` pour le mot de passe root de MySQL.

docker pull mariadb: Télécharge l'image Docker officielle de Mariadb depuis Docker Hub.

docker run --network docker -e MARIADB_ROOT_PASSWORD=myPasswordMariadb --hostname mariadb-container mariadb : Lance un conteneur basé sur l'image `mariadb`, sur le réseau Docker nommé `docker`, avec la variable d'environnement `MARIADB_ROOT_PASSWORD` définie à `myPasswordMariadb`, et définit le nom d'hôte du conteneur à `mariadb-container`.

docker run --network docker --hostname phpmyadmin-container --link mysql-container:db1 --link mariadb-container:db2 -e PMA_ARBITRARY=1 -p 8080:80 phpmyadmin : Lance un conteneur basé sur l'image `phpmyadmin`, sur le réseau Docker nommé `docker`, avec le nom d'hôte `phpmyadmin-container`, lie le conteneur nommé `mysql-container` sous l'alias `db1` et le conteneur nommé `mariadb-container` sous l'alias `db2`, définit la variable d'environnement `PMA_ARBITRARY=1` pour permettre de se connecter à tout serveur de base de données, et expose le port 80 du conteneur sur le port 8080 de l'hôte.

# Docker Postgres + PGAdmin4

docker pull postgres: Télécharge l'image Docker officielle de Postgres depuis Docker Hub.

docker pull dpage/pgadmin4: Télécharge l'image Docker officielle de PGAdmin depuis Docker Hub.

docker run -d --network docker --hostname postgres-database -e POSTGRES_PASSWORD=myPassword postgres : Lance un conteneur en mode détaché (`-d`) basé sur l'image `postgres`, sur le réseau Docker nommé `docker`, avec la variable d'environnement `POSTGRES_PASSWORD` définie à `myPassword`, et définit le nom d'hôte du conteneur à `postgres-database`.

docker run -d --network docker --hostname pgadmin-container --link postgres-database:db -p 8080:80 -e PGADMIN_DEFAULT_EMAIL=default@email.com -e PGADMIN_DEFAULT_PASSWORD=myPassword dpage/pgadmin4 : Lance un conteneur en mode détaché (`-d`) basé sur l'image `dpage/pgadmin4`, sur le réseau Docker nommé `docker`, avec le nom d'hôte `pgadmin-container`, lie le conteneur nommé `postgres-database` sous l'alias `db`, expose le port 80 du conteneur sur le port 8080 de l'hôte, et définit les variables d'environnement `PGADMIN_DEFAULT_EMAIL` et `PGADMIN_DEFAULT_PASSWORD` pour configurer les identifiants de connexion par défaut de pgAdmin.