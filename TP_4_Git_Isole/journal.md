mkdir git_isole
cd git_isole

git init --bare : initialise un dépôt Git bare dans le répertoire `git_isole`. Un dépôt bare ne contient pas de copie de travail des fichiers, il est généralement utilisé comme dépôt central pour le partage.

cd ..

git clone git_isole git_isole2 : Cette commande clone le dépôt bare git_isole dans un nouveau répertoire nommé git_isole2

cd git_isole
git commit -m "Initial empty commit" --allow-empty
cd ..
cd git_isole2
git commit -m "Initial empty commit" --allow-empty
git push