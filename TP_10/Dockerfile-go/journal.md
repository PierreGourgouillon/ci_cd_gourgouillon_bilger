git clone https://github.com/olliefr/docker-gs-ping

cd docker-gs-ping

docker build -t test-go . : Construit une image Docker nommée test-go en utilisant le Dockerfile du répertoire actuel.

docker run -p 8080:8080 test-go : Lance un conteneur à partir de l'image test-go, mappant le port 8080 de l'hôte au port 8080 du conteneur pour permettre l'accès via http://localhost:8080.