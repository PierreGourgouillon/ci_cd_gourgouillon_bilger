docker build -t test-go-multi -f Dockerfile.multistage . : Construit une image Docker nommée test-go-multi en utilisant les instructions du fichier Dockerfile.multistage dans le répertoire courant.

docker run -p 8080:8080 test-go-multi : Lance un conteneur à partir de l'image test-go-multi, mappant le port 8080 de l'hôte au port 8080 du conteneur pour permettre l'accès via http://localhost:8080.