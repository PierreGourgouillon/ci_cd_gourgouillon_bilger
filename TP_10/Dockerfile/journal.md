docker build -t my-reverse-image . : Construit une image Docker nommée my-reverse-image en utilisant les fichiers du répertoire actuel.

docker run --rm my-reverse-image : Exécute un conteneur basé sur l'image my-reverse-image et supprime automatiquement le conteneur après son exécution.