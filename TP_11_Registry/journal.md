docker run -d -p 5000:5000 --name registry registry:2 : Lancer l'image Docker Registry sur le port 5000

echo -e "FROM alpine\nCMD ['echo', 'Hello World']" > Dockerfile : Créer un Dockerfile simple

docker build -t myalpine . : Construire l'image à partir du Dockerfile

docker tag myalpine localhost:5000/myalpine : Taguer l'image pour la registry locale sur le port 5000

docker push localhost:5000/myalpine : Push l'image dans la registry

curl http://localhost:5000/v2/_catalog : Vérifier que l'image est dans la registry

docker pull hello-world : Tirer l'image hello-world depuis Docker Hub

docker tag hello-world localhost:5000/hello-world : Tagger l'image pour la registry locale sur le port 5000

docker push localhost:5000/hello-world : Pousser l'image dans la registry locale

curl http://localhost:5000/v2/_catalog : Vérifier que l'image est dans la registry locale