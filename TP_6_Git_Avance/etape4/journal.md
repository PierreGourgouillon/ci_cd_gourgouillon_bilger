git checkout -b dev HEAD~6
git checkout -b add-file-l2-1
echo "Line 2" >> file1
git add file1
git commit -m "Add line 2 in file1 - Sebastien G"
git merge add-file-l2-1 --no-ff -m "Merge add-file-l2-1 - Sebastien"
git checkout dev
git merge add-file-l2-1 --no-ff -m "Merge add-file-l2-1 - Sebastien"
git checkout -b add-file-l2-2
echo "Line 2" >> file2
git add file2
git commit -m "Add line 2 in file2 - Sebastien G"
git checkout dev
git merge add-file-l2-2 --no-ff -m "Merge add-file-l2-2 - Sebastien"
git checkout -b add-file-l2-3
echo "Line 2" >> file3
git add file3
git commit -m "Add line 2 in file3 - Sebastien"
git checkout dev
git merge add-file-l2-3 --no-ff -m "Merge add-file-l2-3 - Sebastien"