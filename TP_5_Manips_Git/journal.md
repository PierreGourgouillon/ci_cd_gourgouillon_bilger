mkdir test_repo
cd test_repo
git init

git commit -m "initial empty commit" --allow-empty : Cette commande crée un commit initial vide avec le message "initial empty commit".

git remote add origin git@gitlab.com:CB-Info/test_repo.git : Cette commande ajoute un dépôt distant nommé `origin` pointant vers `git@gitlab.com:CB-Info/test_repo.git`.

git push --set-upstream origin main : Cette commande pousse les modifications vers la branche `main` du dépôt distant et configure `main` comme la branche par défaut pour les futurs push.

git archive -o latest`date +%Y%m%d_%H%M%S`.zip HEAD : Cette commande crée une archive ZIP du contenu du dépôt à l'état du commit HEAD et nomme le fichier avec un timestamp.

cd ..

git clone --depth 1 --branch main git@gitlab.com:PierreGourgouillon/ci_cd_gourgouillon_bilger extract : Cette commande clone le dépôt `ci_cd_gourgouillon_bilger` avec seulement le dernier commit de la branche main dans le répertoire extract.


cd ci_cd_gourgouillon_bilger

git show main : Cette commande affiche les détails du dernier commit de la branche main.

git log --oneline : Cette commande affiche l'historique des commits avec une ligne par commit.

git show 2d00513 : Cette commande affiche les détails du commit avec le hachage 2d00513.

git show 2d00513:journal.md : Cette commande affiche le contenu du fichier journal.md au commit avec le hachage 2d00513.
