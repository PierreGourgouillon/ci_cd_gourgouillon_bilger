#!/bin/bash
# dump.sh

mysqldump -h mysql -uroot -pMyPassword --all-databases > /dumps/mysql_dump.sql
mysqldump -h mariadb -uroot -pMyPassword --all-databases > /dumps/mariadb_dump.sql