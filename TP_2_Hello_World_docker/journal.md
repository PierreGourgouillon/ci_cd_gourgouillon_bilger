docker ps : Affiche la liste des conteneurs en cours d'exécution.

docker run hello-world : Teste que Docker fonctionne correctement en exécutant un conteneur simple.

docker run redislabs/redismod : Exécute un conteneur basé sur l'image `redislabs/redismod`.

docker run phisit11/nginx-nodejs-redis-web1:0227 : Exécute un conteneur basé sur l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker run -it phisit11/nginx-nodejs-redis-web1:0227 sh : Lance un conteneur en mode interactif avec un shell (`sh`) à partir de l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker network create docker : Crée un réseau Docker nommé `docker`.

docker run redislabs/redismod : Exécute un conteneur basé sur l'image `redislabs/redismod`.

docker run --network docker phisit11/nginx-nodejs-redis-web1:0227 : Lance un conteneur sur le réseau `docker` avec l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker run -it phisit11/nginx-nodejs-redis-web1:0227 sh : Exécute un conteneur en mode interactif avec un shell (`sh`) à partir de l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker run --network docker phisit11/nginx-nodejs-redis-web1:0227 : Lance un conteneur sur le réseau `docker` avec l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker run --network docker redislabs/redismod : Exécute un conteneur sur le réseau `docker` avec l'image `redislabs/redismod`.

docker run -it phisit11/nginx-nodejs-redis-web1:0227 sh : Lance un conteneur en mode interactif avec un shell (`sh`) à partir de l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker ps : Affiche la liste des conteneurs en cours d'exécution.

docker run --network docker --hostname redis redislabs/redismod : Exécute un conteneur sur le réseau `docker` avec l'image `redislabs/redismod` et le nom d'hôte `redis`.

docker run --network docker --hostname web phisit11/nginx-nodejs-redis-web1:0227 : Lance un conteneur sur le réseau `docker` avec l'image `phisit11/nginx-nodejs-redis-web1:0227` et le nom d'hôte `web`.

docker run -it phisit11/nginx-nodejs-redis-web1:0227 sh : Exécute un conteneur en mode interactif avec un shell (`sh`) à partir de l'image `phisit11/nginx-nodejs-redis-web1:0227`.

docker run --network docker --hostname redis -p 6379:6379 redislabs/redismod : Exécute un conteneur sur le réseau `docker`, expose le port 6379 et utilise le nom d'hôte `redis`.

docker run --network docker --hostname web phisit11/nginx-nodejs-redis-web1:0227 : Lance un conteneur sur le réseau `docker` avec l'image `phisit11/nginx-nodejs-redis-web1:0227` et le nom d'hôte `web`.

docker run --network docker --hostname web -p 80:5000 phisit11/nginx-nodejs-redis-web1:0227 : Exécute un conteneur sur le réseau `docker`, expose le port 80 et utilise le nom d'hôte `web`.

curl localhost : Vérifie que le serveur web fonctionne en envoyant une requête HTTP à `localhost`.

docker run --network docker --hostname redis -p 6379:6379 redislabs/redismod : Exécute un conteneur sur le réseau `docker`, expose le port 6379 et utilise le nom d'hôte `redis`.

docker run --network docker -v redis_data:/data --hostname redis -p 6379:6379 redislabs/redismod : Exécute un conteneur sur le réseau `docker`, monte le volume `redis_data`, expose le port 6379 et utilise le nom d'hôte `redis`.

docker run --network docker --hostname web -p 8080:5000 phisit11/nginx-nodejs-redis-web1:0227 : Lance un conteneur sur le réseau `docker`, expose le port 8080 et utilise le nom d'hôte `web`.

curl localhost : Vérifie que le serveur web fonctionne en envoyant une requête HTTP à `localhost`.

docker run --network docker --hostname web -p 8080:5000 phisit11/nginx-nodejs-redis-web1:0227 : Exécute un conteneur sur le réseau `docker`, expose le port 8080 et utilise le nom d'hôte `web`.

docker run -d --network docker -v redis_data:/data --hostname redis -p 6379:6379 redislabs/redismod : Lance un conteneur en mode détaché sur le réseau `docker`, monte le volume `redis_data`, expose le port 6379 et utilise le nom d'hôte `redis`.

docker run -d --network docker --hostname web -p 8080:5000 phisit11/nginx-nodejs-redis-web1:0227 : Lance un conteneur en mode détaché sur le réseau `docker`, expose le port 8080 et utilise le nom d'hôte `web`.

docker ps : Affiche la liste des conteneurs en cours d'exécution.

docker stop f9a86b73f14e : Arrête le conteneur avec l'ID `f9a86b73f14e`.

docker stop 8ad5e1b4b20e : Arrête le conteneur avec l'ID `8ad5e1b4b20e`.

docker ps : Affiche la liste des conteneurs en cours d'exécution.

docker logs 8ad5e1b4b20e : Affiche les logs du conteneur avec l'ID `8ad5e1b4b20e`.

docker run -it --network docker -v redis_data:/data --hostname ubuntu ubuntu : Exécute un conteneur interactif avec un volume monté sur le réseau `docker` en utilisant l'image `ubuntu`.