# Etape A
echo "Deuxieme commit" > file.txt: Ajouter une ligne dans le fichier `file.txt`
git add file.txt: Ajouter le fichier au commit
git commit -m "Deuxieme commit": Créer le commit avec le nom `Deuxieme commit`
echo "Troisieme commit" > file.txt
git add file.txt
git commit -m "Troisieme commit"
git push: Envoyer le commit sur le serveur
git checkout -b dev: Création de la branche `dev` et aller dessus
echo "Premier commit dev" > file.txt
git add file.txt
git commit -m "Premier commit dev"
git push
git push --set-upstream origin dev
git checkout main
echo "Quatrieme commit main" > file.txt
git add file.txt
git commit -m "Quatrieme commit main"
git push
git checkout dev
echo "Deuxieme commit dev" > file.txt
git add file.txt
git commit -m "Deuxieme commit dev"
git push
git checkout -b green-branch HEAD~1
echo "Premier commit green" > file.txt
git add file.txt
git commit -m "Premier commit green"
git push
git push --set-upstream origin green-branch
git checkout dev
echo "Troisieme commit dev" > file.txt
git add file.txt
git commit -m "Troisieme commit dev"
git push
git checkout main
echo "Cinquieme commit main" > file.txt
git ad file.txt
git add file.txt
git commit -m "Cinquieme commit main"
git push
git checkout dev
git checkout -b purple-branch
echo "Premier commit purple" > file.txt
git add file.txt
git commit -m "Premier commit purple"
git push
git push --set-upstream origin purple-branch
git checkout green-branch
echo "Deuxieme commit green" > file.txt
git add file.txt
git commit -m "Deuxieme commit green"
git push
git checkout main
echo "Sixieme commit main" > file.txt
git add file.txt
git commit -m "Sixieme commit main"
git push
git checkout green-branch
echo "Troisieme commit green" > file.txt
git add file.txt
git commit -m "Troisieme commit green"
git push
git checkout purple-branch
echo "Deuxieme commit purple" > file.txt
git add file.txt
git commit -m "Deuxieme commit purple"
git push
git checkout green-branch
git checkout dev
git checkout main
git merge green-branch
git commit -m "Resolve conflicts"
git push
git checkout dev
echo "Quatrieme commit dev" > file.txt
git add file.txt
git commit -m "Quatrieme commit dev"
git push
git checkout purple-branch
echo "Troisieme commit purple" > file.txt
git add file.txt
git commit -m "Troisieme commit purple"
git push

# Etape B

echo "Deuxieme commit main" > file.txt
git add file.txt
git commit -m "Deuxieme commit main"
git push
echo "Troisieme commit main" > file.txt
git add file.txt
git commit -m "Troisieme commit main"
git push
git checkout -b green-branch
echo "Premier commit green" > file.txt
git add file.txt
git commit -m "Premier commit green"
git push
git push --set-upstream origin green-branch
git checkout main
echo "Quatrieme commit main" > file.txt
git add file.txt
git commit -m "Quatrieme commit main"
git push
git checkout green-branch
echo "Deuxieme commit green" > file.txt
git add file.txt
git commit -m "Deuxieme commit green"
git push
git checkout main
echo "cinquieme commit main" > file.txt
git add file.txt
git commit -m "Cinquieme commit main"
git push
git checkout green-branch
echo "3 commit green" > file.txt
git add file.txt
git commit -m "3 commit green"
git push
echo "4 commit green" > file.txt
git add file.txt
git commit -m "4 commit green"
git push
git checkout main
echo "6 commit main" > file.txt
git add file.txt
git commit -m "6 commit main"
git push
git checkout green-branch
echo "5 commit green" > file.txt
git add file.txt
git commit -m "5 commit green"
git push
git checkout main
git rebase green-branch
git commit -m "Resolve conflicts green"
git push
git add file.txt
git rebase --continue
git commit -m "Resolve conflicts"
git push
git push -f
git checkout -b blue-branch
echo "1 commit blue" > file.txt
git add file.txt
git commit -m "1 commit blue"
git push
git push --set-upstream origin blue-branch
git checkout -b purple
echo "1 commit purple" > file.txt
git add file.txt
git commit -m "1 commit purple"
git push
git push --set-upstream origin purple
echo "2 commit purple" > file.txt
git add file.txt
git commit -m "2 commit purple"
git push
git checkout blue-branch
echo "2 commit blue" > file.txt
git add file.txt
git commit -m "2 commit blue"
git push
git checkout purple
echo "3 commit purple" > file.txt
git add file.txt
git commit -m "3 commit purple"
git push

# Etape C

touch file.txt
git add file.txt
git commit -m "Second commit"
git push
git commit --allow-empty -m "Third commit"
git push
git checkout -b green-branch
git commit --allow-empty -m "Dev A"
git push
git push --set-upstream origin green-branch
git commit --allow-empty -m "Dev B"
git push
git checkout main
git commit --allow-empty -m "main D"
git push
git checkout green-branch
git commit --allow-empty -m "Dev C"
git push
git checkout main
git commit --allow-empty -m "main E"
git push
git checkout green-branch
git commit --allow-empty -m "Dev D"
git push
git checkout main
git commit --allow-empty -m "main F"
git push
git checkout green-branch
git commit --allow-empty -m "Dev E"
git push
git checkout main
git merge green-branch -m "Merge green"
git push
git checkout blue-branch
git commit --allow-empty -m "Blue A"
git push
git commit --allow-empty -m "Blue B"
git checkout -b blue-branch
git commit --allow-empty -m "Blue A"
git commit --allow-empty -m "Blue B"
git push
git push --set-upstream origin blue-branch
git checkout main
git merge blue-branch -m "Merge blue"
git push
git checkout -b purple-branch
git commit --allow-empty -m "Purple A"
git push --set-upstream origin purple-branch
git commit --allow-empty -m "Purple B"
git push
git commit --allow-empty -m "Purple C"
git push